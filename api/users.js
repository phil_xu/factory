const models = require('../models')
const express = require('express')
const router = express.Router()
const Op = require('sequelize').Op
const crypto = require('crypto')
var moment = require('moment')
var jwt = require('jwt-simple')
const secret = 'mlab'
router.get('/', function (req, res, next) {
  var where = {}
  Object.assign(where, req.query)
  console.log(where)
  if (req.query.producing) {
    delete where.producing
    where['status'] = {
      [Op.or]: ['喷蜡', '浇筑', '执模', '字印', '质检']
    }
  }
  console.log(where)
  models.Order.findAndCountAll({
    where: where
  }).then(function (orders) {
    res.send(orders.rows)
  })
})
router.post('/login', async function (req, res, next) {
  try {
    var user = await models.User.findOne({
      where: {
        name: req.body.name
      }
    })
    if (!user) {
      res.send({
        error: {
          message: '无此用户'
        }
      })
    } else {
      const hash = crypto.createHmac('sha256', secret)
        .update(req.body.password)
        .digest('hex')
      if (hash === user.password) {
        var expires = moment().add('days', 7).valueOf()
        var token = jwt.encode({
          iss: user.id,
          role: user.role,
          exp: expires
        }, secret)
        console.log(token)
        await user.update({
          token: token
        })
        res.send({
          role: user.role,
          factory: user.factory,
          token: token
        })
      } else {
        res.send({
          error: {
            message: '密码错误'
          }
        })
      }
    }
  } catch (e) {
    console.log(e.message)
    res.send({
      error: {
        message: e.message
      }
    })
  }
})
router.post('/register', async function (req, res, next) {
  const hash = crypto.createHmac('sha256', secret)
    .update(req.body.password)
    .digest('hex')
  var newUser = {
    name: req.body.name,
    password: hash
  }
  try {
    var user = await models.User.findOne({
      where: {
        name: req.body.name
      }
    })
    if (user) {
      res.send({
        error: {
          message: '用户已存在'
        }
      })
    } else {
      await models.User.create(newUser)
      res.send({})
    }
  } catch (e) {
    res.send({
      error: {
        message: '新建用户失败'
      }
    })
  }
})

module.exports = router
