var models = require('../models')
var express = require('express')
var router = express.Router()
router.post('/', async function (req, res, next) {
  console.log(req.body)
  var list = req.body
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    try {
      var order = await models.Order.findOne({
        where: {
          itemId: item.itemId
        }
      })
      if (!order) {
        order = await models.Order.create(item)
      } else {
        order.update(item)
      }
    } catch (e) {
      console.log(e)
      res.send({
        error: {
          message: '提交失败'
        }
      })
      return
    }
  }
  res.send({})
})

module.exports = router
