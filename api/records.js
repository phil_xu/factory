var models = require('../models')
var express = require('express')
var moment = require('moment')
var router = express.Router()
const Op = require('sequelize').Op
router.get('/', function (req, res, next) {
  var from = moment().subtract(14, 'days').valueOf()
  var where = {
    createdAt: {
      [Op.gt]: from
    }
  }
  models.Record.findAndCountAll({
    where: where,
    include: [{
      model: models.User,
      as: 'user',
      attributes: ['name']
    }, {
      model: models.Order,
      as: 'order',
      attributes: ['itemId']
    }],
    order: [
      ['createdAt', 'DESC']
    ],
    raw: true
  }).then(function (records) {
    res.send(records.rows)
  })
})
module.exports = router
