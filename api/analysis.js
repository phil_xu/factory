var models = require('../models')
var express = require('express')
var router = express.Router()
const Op = require('sequelize').Op
function buildWhere(options, where = {}) {
  if (options.type) {
    where.type = options.type
  }
  if (options.shopId) {
    where.shopId = options.shopId
  }
  if (options.startTime) {
    where.createdAt = {}
    where.createdAt[Op.gt] = options.startTime
  }
  if (options.endTime) {
    if (!where.createdAt) where.createdAt = {}
    where.createdAt[Op.lte] = options.endTime
  }
  return where
}
// 库存报表
router.get('/stock-detail', async function(req, res, next) {
  // 起始库存
  var where = {}
  var options = req.query
  if (options.shopId) {
    where.shopId = options.shopId
  }
  options.startTime = options.startTime || new Date()
  if (options.startTime) {
    where.createdAt = {}
    where.createdAt[Op.lte] = options.startTime
  }
  var attributes = ['productId', [models.sequelize.fn('sum', models.sequelize.col('quantity')), 'quantity'], [models.sequelize.fn('sum', models.sequelize.col('price')), 'price']]
  var include = [{
    model: models.Product,
    as: 'product',
    attributes: ['item_title', 'item_sub_attr', 'item_sn', 'item_barcode', 'item_show_pic', 'item_price']
  }]
  var stockDetail = {}
  var stocks = await models.Stock.findAll({
    where: where,
    group: 'productId',
    attributes: attributes,
    include: include
  })
  stocks.forEach(s => {
    console.log(s.product)
    stockDetail[s.productId] = {
      startQuantity: s.quantity,
      endQuantity: s.quantity,
      startMoney: s.price,
      endMoney: s.price
    }
    Object.assign(stockDetail[s.productId], s.product.dataValues)
  })
  var orders = await models.Order.findAll({
    where: where,
    group: 'productId',
    attributes: attributes,
    include: include
  })
  orders.forEach(s => {
    stockDetail[s.productId].startQuantity -= s.quantity
    stockDetail[s.productId].startMoney -= s.price
    stockDetail[s.productId].endQuantity -= s.quantity
    stockDetail[s.productId].endMoney -= s.price
  })
  where['status'] = 'returned'
  var services = await models.Service.findAll({
    where: where,
    group: 'productId',
    attributes: attributes,
    include: include
  })
  services.forEach(s => {
    stockDetail[s.productId].startQuantity -= s.quantity
    stockDetail[s.productId].startMoney -= s.price
    stockDetail[s.productId].endQuantity -= s.quantity
    stockDetail[s.productId].endMoney -= s.price
  })
  // 该期间库存变化
  where = {}
  if (options.shopId) {
    where.shopId = options.shopId
  }
  if (options.startTime) {
    where.createdAt = {}
    where.createdAt[Op.gt] = options.startTime
  }
  if (options.endTime) {
    if (!where.createdAt) where.createdAt = {}
    where.createdAt[Op.lte] = options.endTime
  }
  stocks = await models.Stock.findAll({
    where: where,
    group: 'productId',
    attributes: attributes,
    include: include
  })
  stocks.forEach(s => {
    stockDetail[s.productId] = stockDetail[s.productId] || {}
    Object.assign(stockDetail[s.productId], s.product.dataValues)
    stockDetail[s.productId]['stock'] = s.quantity
    stockDetail[s.productId]['stockMoney'] = s.price
    stockDetail[s.productId].endQuantity = stockDetail[s.productId].endQuantity ? stockDetail[s.productId].endQuantity - s.quantity : s.quantity
    stockDetail[s.productId].endMoney = stockDetail[s.productId].endMoney ? stockDetail[s.productId].endMoney - s.price : s.price
  })
  orders = await models.Order.findAll({
    where: where,
    group: 'productId',
    attributes: attributes,
    include: include
  })
  orders.forEach(s => {
    stockDetail[s.productId]['order'] = s.quantity
    stockDetail[s.productId]['orderMoney'] = s.price
    stockDetail[s.productId].endQuantity -= s.quantity
    stockDetail[s.productId].endMoney -= s.price
  })
  where['status'] = 'returned'
  services = await models.Service.findAll({
    where: where,
    group: 'productId',
    attributes: attributes,
    include: include
  })
  services.forEach(s => {
    stockDetail[s.productId]['returned'] = s.quantity
    stockDetail[s.productId]['returnedMoney'] = s.price
    stockDetail[s.productId].endQuantity -= s.quantity
    stockDetail[s.productId].endMoney -= s.price
  })
  res.send(stockDetail)
})

router.get('/shop-count', function(req, res, next) {
  models.Shop.count().then(function(count) {
    res.send({
      count: count
    })
  })
})
router.get('/shop-layout', function(req, res, next) {
  var by = req.query.by || 'province'
  models.Shop.findAll({
    attributes: [[by, 'scope'], [models.sequelize.fn('count', '*'), 'count']],
    group: by
  }).then(function(shops) {
    res.send(shops)
  })
})
// 销售额统计
router.get('/price-layout', function(req, res, next) {
  var where = buildWhere(req.query)
  var by = req.query.by || 'province'
  models.Shop.findAll({
    where: where,
    include: [{
      model: models.Order,
      as: 'orders',
      attributes: []
    }],
    group: by,
    attributes: [[by, 'scope'], [models.sequelize.fn('sum', models.sequelize.col('orders.price')), 'total']],
    order: [[models.sequelize.col('total'), 'DESC']]
  }).then(function(orders) {
    res.send(orders)
  })
})
router.get('/service-count', function(req, res, next) {
  models.Service.count().then(function(count) {
    res.send({
      count: count
    })
  })
})
// 退换货产品
router.get('/service-list', function(req, res, next) {
  var where = buildWhere(req.query)
  models.Service.findAll({
    where: where,
    include: [{
      model: models.Product,
      as: 'product',
      attributes: ['item_title', 'item_sub_attr']
    }],
    attributes: ['productId', 'status', [models.sequelize.fn('count', '*'), 'count']],
    group: ['productId', 'status']
  }).then(function(orders) {
    res.send(orders)
  })
})
// 退货数量
router.get('/service-returned', function(req, res, next) {
  var where = buildWhere(req.query, {
    status: 'returned'
  })
  models.Service.findAll({
    where: where,
    include: [{
      model: models.Product,
      as: 'product',
      attributes: ['item_title', 'item_show_pic', 'item_sub_attr']
    }],
    attributes: ['productId', [models.sequelize.fn('SUM', models.sequelize.col('quantity')), 'count']],
    group: ['productId'],
    order: [
      ['id', 'DESC']
    ]
  }).then(function(services) {
    // console.log(services)
    res.send(services)
  })
})
router.get('/total-price', function(req, res, next) {
  models.Order.sum('price').then(function(total) {
    res.send({
      total: total
    })
  })
})
// 总加购数量
router.get('/total-quantity', function(req, res, next) {
  models.Order.sum('quantity').then(function(total) {
    res.send({
      total: total
    })
  })
})
// 总加购次数
router.get('/total-times', function(req, res, next) {
  models.Order.count().then(function(total) {
    res.send({
      total: total
    })
  })
})
// router.get('/top-price-product', function(req, res, next) {
//   models.Order.findOne({
//     attributes: ['productId', [models.sequelize.fn('SUM', models.sequelize.col('price')), 'total']],
//     group: 'productId',
//     include: [{
//       model: models.Product,
//       as: 'product',
//       attributes: ['item_title', 'item_show_pic', 'item_sub_attr']
//     }],
//     order: [
//       [models.sequelize.col('total'), 'DESC']
//     ]
//   }).then(function(order) {
//     res.send(order)
//   })
// })
// 单品加购数量
router.get('/top-quantity-product', function(req, res, next) {
  models.Order.findOne({
    attributes: ['productId', [models.sequelize.fn('SUM', models.sequelize.col('quantity')), 'total']],
    include: [{
      model: models.Product,
      as: 'product',
      attributes: ['item_title', 'item_show_pic', 'item_sub_attr']
    }],
    group: 'productId',
    order: [
      [models.sequelize.col('total'), 'DESC']
    ]
  }).then(function(order) {
    res.send(order)
  })
})
// 单品最高加购次数
router.get('/top-times-product', function(req, res, next) {
  models.Order.findOne({
    attributes: ['productId', [models.sequelize.fn('COUNT', '*'), 'count']],
    include: [{
      model: models.Product,
      as: 'product',
      attributes: ['item_title', 'item_show_pic', 'item_sub_attr']
    }],
    group: 'productId',
    order: [
      [models.sequelize.col('count'), 'DESC']
    ]
  }).then(function(order) {
    res.send(order)
  })
})
// 单品销量统计
router.get('/quantity-product-list', function(req, res, next) {
  var where = buildWhere(req.query)
  models.Order.findAll({
    where: where,
    attributes: ['productId', [models.sequelize.fn('SUM', models.sequelize.col('quantity')), 'count']],
    include: [{
      model: models.Product,
      as: 'product',
      attributes: ['item_title', 'item_show_pic', 'item_sub_attr']
    }],
    group: 'productId',
    order: [
      [models.sequelize.col('count'), 'DESC']
    ]
  }).then(function(orders) {
    res.send(orders)
  })
})
// 下单次数统计
router.get('/times-product-list', function(req, res, next) {
  var where = buildWhere(req.query)
  models.Order.findAll({
    where: where,
    attributes: ['productId', [models.sequelize.fn('COUNT', '*'), 'count']],
    include: [{
      model: models.Product,
      as: 'product',
      attributes: ['item_title', 'item_show_pic', 'item_sub_attr']
    }],
    group: 'productId',
    order: [
      [models.sequelize.col('count'), 'DESC']
    ]
  }).then(function(orders) {
    res.send(orders)
  })
})
// 单品区域统计
router.get('/product-in-province', function(req, res, next) {
  var where = buildWhere(req.query)
  models.Order.findAll({
    where: where,
    attributes: ['productId', [models.sequelize.fn('COUNT', '*'), 'count']],
    include: [{
      model: models.Product,
      as: 'product',
      attributes: ['item_title', 'item_show_pic', 'item_sub_attr']
    }, {
      model: models.Shop,
      as: 'shop',
      attributes: ['province']
    }],
    group: ['productId', models.sequelize.col('shop.province')],
    order: [
      [models.sequelize.col('count'), 'DESC']
    ]
  }).then(function(orders) {
    res.send(orders)
  })
})
router.get('/top-price-zone', function(req, res, next) {
  // models.Shop.hasMany(models.Order)
  models.Shop.findAll({
    include: [{
      model: models.Order,
      as: 'orders',
      attributes: []
    }],
    group: 'zone',
    attributes: ['zone', [models.sequelize.fn('SUM', models.sequelize.col('orders.price')), 'total']]
  }).then(function(orders) {
    res.send(orders)
  })
})
router.get('/top-price-province', function(req, res, next) {
  models.Shop.findAll({
    include: [{
      model: models.Order,
      as: 'orders',
      attributes: []
    }],
    group: 'province',
    attributes: ['province', [models.sequelize.fn('sum', models.sequelize.col('orders.price')), 'total']],
    order: [[models.sequelize.col('total'), 'DESC']]
  }).then(function(orders) {
    res.send(orders[0])
  })
})
module.exports = router
