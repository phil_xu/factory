module.exports = function (sequelize, DataTypes) {
  var Record = sequelize.define('Record', {
    id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
    operation: {
      type: DataTypes.STRING(1024),
      defaultValue: '{}',
      get: function () { return JSON.parse(this.getDataValue('operation')) },
      set: function (json) { this.setDataValue('operation', JSON.stringify(json)) }
    }
  }, {
    tableName: 'records'
  })
  Record.associate = function (models) {
    Record.belongsTo(models.User, { as: 'user', foreignKey: 'userId', targetKey: 'id' })
    Record.belongsTo(models.Order, { as: 'order', foreignKey: 'orderId', targetKey: 'id' })
  }
  return Record
}
