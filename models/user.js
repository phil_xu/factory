module.exports = function (sequelize, DataTypes) {
  var User = sequelize.define('User', {
    id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
    name: { type: DataTypes.STRING(16) },
    password: DataTypes.STRING(64),
    role: { type: DataTypes.STRING(16) },
    token: DataTypes.STRING(256),
    factory: DataTypes.STRING(64)
  }, {
    tableName: 'users'
  })
  User.associate = function (models) {
    // Order.belongsTo(models.Shop, { as: 'shop', foreignKey: 'shopId', targetKey: 'id' })
    // Order.belongsTo(models.Product, { as: 'product', foreignKey: 'productId', targetKey: 'item_id' })
    User.hasMany(models.Record, { as: 'records', foreignKey: 'userId', sourceKey: 'id' })
    // Shop.hasMany(models.Order, { as: 'orders', foreignKey: 'shopId', sourceKey: 'id' })
  }
  return User
}
